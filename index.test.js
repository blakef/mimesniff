const extensionToMIME = {
  // Audio
  'aiff': 'audio/aiff',
  'mp3':  'audio/mpeg',
  'ogg':  'application/ogg',
  'midi': 'audio/midi',
  'avi':  'video/avi',
  'wav':  'audio/wave',

  // Image
  'ico':  'image/x-icon',
  'bmp':  'image/bmp',
  'gif':  'image/gif',
  'webp': 'image/webp',
  'png':  'image/png',
  'jpg':  'image/jpeg',

  // Archive
  'gz':   'application/x-gzip',
  'zip':  'application/zip',
  'rar':  'application/x-rar-compressed',

  // Documents
  'ps':   'application/postscript',
  'pdf':  'application/pdf',
};

const files = [
  'tower.bmp',
  'tower.jpg',
  'tower.png',
  'tower.gz',
  'tower.webp',
  'tower.zip',
];

describe('creates no false negatives', () => {

  // We know that all of these file extensions are accurate, so generate a bunch
  // of tests for them.
  files.forEach(fileName => {
    const ext = fileName.split('.').slice(-1);
    const out = extensionToMIME[ext];

    if (!out) {
      it(`should identify '${getMIMEType.UNKNOWN}' from '${fileName}`);
    } else {
      it(`should identify '${out}' from '${fileName}`, (done) => {
        fetch('data/' + fileName)
          .then(r => r.arrayBuffer())
          .then(getMIMEType)
          .then(mime => {
            mime.should.eql(out);
            done();
          }, err => done(new Error(err)));
      });
    }
    
  });

});
